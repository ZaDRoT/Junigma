module Rotor

import Random

"""
Structure of rotor and constructors
"""
mutable struct Struct{T <: Unsigned}
    turns::T
    wheel::Vector{T}

    function Struct{T}(turns, wheel::Vector) where {T <: Unsigned}
        @assert length(wheel) > 0 "Zero length"
        @assert turns < length(wheel) "Turns value"
        new(turns, wheel)
    end
end

function Struct{T}(length) where {T <: Unsigned}
    Struct{T}(0, Random.shuffle!(Vector{T}(0:length-1)))
end

function Struct(rotor::Struct{T}) where {T <: Unsigned}
    Struct{T}(rotor.turns, rotor.wheel)
end

function Struct{T}(rotor::Struct{D}) where {T <: Unsigned, D <: Unsigned}
    Struct{T}(rotor.turns, rotor.wheel)
end

"""
Set position of rotor wheel
"""
function set(rotor::Struct{T}, turns::T)::T where {T <: Unsigned}
    @assert turns < length(wheel) "Turns value"
    rotor.turns = turns
end

function set(rotor::Struct{T}, turns)::T where {T <: Unsigned}
    set(rotor::Struct{T}, T(turns))
end

"""
Turn rotor with C style overflow and return signal of it
"""
function turn(rotor::Struct{T})::Bool where {T <: Unsigned}
    rotor.turns += T(1)
    rotor.turns %= length(rotor.wheel)
    rotor.turns == 0
end

"""
Convert value A -> B
"""
function forwardStroke(rotor::Struct{T}, value::T)::T where {T <: Unsigned}
    rotor.wheel[(value + rotor.turns) % length(rotor.wheel) + 1]
end

function forwardStroke(rotor::Struct{T}, value)::T where {T <: Unsigned}
    forwardStroke(rotor::Struct{T}, convert(T, value))
end

"""
Convert value A <- B
"""
function reverseStroke(rotor::Struct{T}, value::T)::T where {T <: Unsigned}
    index = findfirst(x->x == value, rotor.wheel)
    if index <=  rotor.turns
        index += length(rotor.wheel) - rotor.turns
    else
        index -= rotor.turns
    end

    return index - 1
end

function reverseStroke(rotor::Struct{T}, value)::T where {T <: Unsigned}
    reverseStroke(rotor::Struct{T}, convert(T, value))
end

"""
Convert value with chosen direction (:forward or :reverse)
"""
function stroke(rotor::Struct{T}, value::T, direction::Symbol)::T where {T <: Unsigned}
    if direction == :forward
        forwardStroke(rotor::Struct{T}, value::T)
    elseif direction == :reverse
        reverseStroke(rotor::Struct{T}, value::T)
    end
end

function stroke(rotor::Struct{T}, value, direction::Symbol)::T where {T <: Unsigned}
    stroke(rotor::Struct{T}, convert(T, value), direction::Symbol)
end

end