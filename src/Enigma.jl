module Enigma

include("Rotor.jl")
import .Rotor

include("Reflector.jl") 
import .Reflector

"""
Structure of Enigma machine and constructors
"""
mutable struct Struct{T <: Unsigned}
    rotors::Vector{Rotor.Struct{T}}
    reflector::Reflector.Struct{T}

    function Struct{T}(rotors::Vector, reflector) where {T <: Unsigned}
        @assert length(rotors) > 0 "Zero length"
        new(rotors, reflector)
    end
end
   
function Struct{T}(alphabetLength, rotorsNumber) where {T <: Unsigned}
    Struct{T}(
                [Rotor.Struct{T}(alphabetLength) for x = 1:rotorsNumber],
                Reflector.Struct{T}(alphabetLength)
             )
end

"""
Cascade turn of rotors
"""
function turn(enigma::Struct{T})::Nothing where {T <: Unsigned}
    for i = 1:length(enigma.rotors)
        if ! Rotor.turn(enigma.rotors[i])
            break
        end
    end
end

"""
Convert data with enigma machine
"""
function enigmatize(enigma::Struct{T}, data::Vector{T})::Vector{T} where {T <: Unsigned}
    res = Vector{T}()

    for symbol in data
        push!(res, symbol)

        for i = 1:length(enigma.rotors)
            res[end] = Rotor.stroke(enigma.rotors[i], res[end], :forward)
        end

        res[end] = Reflector.reciprocate(enigma.reflector, res[end])

        for i = length(enigma.rotors):-1:1
            res[end] = Rotor.stroke(enigma.rotors[i], res[end], :reverse)
        end

        turn(enigma)
    end
    return res
end

function enigmatize(enigma::Struct{T}, data::Vector)::Vector{T} where {T <: Unsigned} 
    enigmatize(enigma::Struct{T}, convert(Vector{T}, data))
end

#function enigmatize(enigma::Struct{T}, data)::Vector{T} where {T <: Unsigned} 
#    # data convertion to array of T
#end

end