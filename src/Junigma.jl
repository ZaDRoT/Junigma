module Junigma

include("Enigma.jl")
import .Enigma

import JLD2

#function write(io::Base.IO, enigma::Enigma.Struct, data)
#    # 
#end

"""
JLD2 save and load of Enigma machine
"""
function save_JLD2(enigma::Enigma.Struct, fileName::String)
    JLD2.jldsave(fileName; enigma)
end

function load_JLD2(fileName::String)::Enigma.Struct
    JLD2.jldopen(fileName, "r") do file
        file["enigma"]
    end
end

end # module
