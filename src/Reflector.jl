module Reflector

"""
Structure of reflector and constructors
"""
mutable struct Struct{T <: Unsigned}
    pairs::Vector{Pair{T, T}}

    function Struct{T}(pairs::Vector) where {T <: Unsigned}
        @assert length(pairs) > 0 "Zero length"
        new(pairs)
    end
end

function Struct{T}(length) where {T <: Unsigned}
    @assert length > 0 "Zero length"

    pairs = []

    i = 0
    while i < length ÷ 2
        push!(pairs, Pair(i, length - i - 1))
        i += 1
    end
    if mod(length, 2) == 1
        push!(pairs, Pair(i, i))
    end

    return Struct{T}(pairs::Vector)
end

function Struct(reflector::Struct{T}) where {T <: Unsigned}
    Struct{T}(reflector.pairs)
end

function Struct{T}(reflector::Struct{D}) where {T <: Unsigned, D <: Unsigned}
    Struct{T}(reflector.pairs)
end

"""
Convert value A -> B or B -> A
"""
function reciprocate(reflector::Struct{T}, value::T) where {T <: Unsigned}
    index = findfirst(x->x.first == value, reflector.pairs)
    if isnothing(index)
        index = findfirst(x->x.second == value, reflector.pairs)
        return reflector.pairs[index].first
    else
        return reflector.pairs[index].second
    end
end

end