using Test
import Junigma

@testset "Rotor" begin
    @testset "Constructors" begin
        @test_throws AssertionError("Zero length") Junigma.Enigma.Rotor.Struct{UInt8}(0, [])
        @test_throws AssertionError("Turns value") Junigma.Enigma.Rotor.Struct{UInt8}(1, [1])


        innerConstructions = 
        [
            Junigma.Enigma.Rotor.Struct{UInt8}(0, [0, 1, 2, 3, 4])
            Junigma.Enigma.Rotor.Struct{UInt16}(1, [0, 1, 2, 3, 4])
            Junigma.Enigma.Rotor.Struct{UInt32}(2, [0, 1, 2, 3, 4])
            Junigma.Enigma.Rotor.Struct{UInt64}(3, [0, 1, 2, 3, 4])
        ]

        @test innerConstructions[1].wheel == [0, 1, 2, 3, 4]
        @test innerConstructions[1].turns == 0
        @test innerConstructions[2].wheel == [0, 1, 2, 3, 4]
        @test innerConstructions[2].turns == 1
        @test innerConstructions[3].wheel == [0, 1, 2, 3, 4]
        @test innerConstructions[3].turns == 2
        @test innerConstructions[4].wheel == [0, 1, 2, 3, 4]
        @test innerConstructions[4].turns == 3


        lengthConstructions =
        [
            Junigma.Enigma.Rotor.Struct{UInt8}(1)
            Junigma.Enigma.Rotor.Struct{UInt8}(2)
        ]

        @test lengthConstructions[1].wheel == [0]
        @test lengthConstructions[1].turns == 0
        @test lengthConstructions[2].wheel == [0, 1] || lengthConstructions[2].wheel == [1, 0]
        @test lengthConstructions[2].turns == 0


        copyConstructions =
        [
            Junigma.Enigma.Rotor.Struct{UInt8}(innerConstructions[1])
            Junigma.Enigma.Rotor.Struct{UInt8}(lengthConstructions[1])
        ]

        @test copyConstructions[1].wheel == [0, 1, 2, 3, 4]
        @test copyConstructions[1].turns == 0
        @test copyConstructions[2].wheel == [0]
        @test copyConstructions[2].turns == 0

        convertConstructions =
        [
            Junigma.Enigma.Rotor.Struct{UInt16}(innerConstructions[1])
            Junigma.Enigma.Rotor.Struct{UInt16}(lengthConstructions[1])
        ]

        @test convertConstructions[1].wheel == [0, 1, 2, 3, 4]
        @test convertConstructions[1].turns == 0
        @test typeof(convertConstructions[1]) == Junigma.Enigma.Rotor.Struct{UInt16}
        @test typeof(convertConstructions[1].wheel) == Vector{UInt16}
        @test typeof(convertConstructions[1].turns) == UInt16
        @test convertConstructions[2].wheel == [0]
        @test convertConstructions[2].turns == 0
        @test typeof(convertConstructions[2]) == Junigma.Enigma.Rotor.Struct{UInt16}
        @test typeof(convertConstructions[2].wheel) == Vector{UInt16}
        @test typeof(convertConstructions[2].turns) == UInt16
    end

    @testset "Set" begin
        @test 1==1
    end
end